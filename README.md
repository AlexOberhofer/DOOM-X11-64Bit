## Overview

This repository contains a building version of the open source release of Linuxdoom v1.10. In order to
create a working binary, a number of changes had to be made to the source. Many changes were derived from 
the VROOM macosx source port. The makefile has been updated and no longer requires 32 bit librarys, nor creates a
32 bit binary. The source for VROOM can be found [here.](https://github.com/Vraiment/VROOM)

This entire project has been entirely for entertainment/educational purposes. Hopefully the contents of this repository 
can help someone get up and going with their own source port. I plan on replacing the X11 library with something more modern,
but that work will be in a different repository.

## Acknowledgements

If for nothing more than an interesting read - [VROOM Development Blog](https://vraimentmxblog.wordpress.com/)

Thank you to ID Software for releasing the Source to Linux Doom v1.10 - [DOOM Source](https://github.com/id-Software/DOOM)

## License 

This project is licensed under the GNU GPL v2.0 License - see the [LICENSE.md](LICENSE.md) file for details